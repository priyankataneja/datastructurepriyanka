package Tree;

public class BinarySearchTree {

	public Node insert(Node node , int val){
		Node a = null;
		if(node == null) {
			 a = new Node(val);
			return a;
		} 
		
		if(val<node.data){
			node.left = insert(node.left, val);
		} else if(val>node.data){
			node.right = insert(node.right, val);
		}
		
		return node;
	}
	
	int findMaxInBST(Node root){
		int max;
		if(root.getRight()!=null){
		 max = findMaxInBST(root.right);
		} else {
			max = root.data;
		}
//		int max = root.data;
//		System.out.println("Value of max is----:"+ max);
		return max;
	}
	
	int findMinInBST(Node root) {
		if(root.getLeft()!=null){
			return findMinInBST(root.left);
		}
		int min = root.data;
		System.out.println("Value of min is===:"+ min);
		return min;
	}
}
