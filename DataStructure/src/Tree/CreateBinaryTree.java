package Tree;
import java.util.ArrayDeque;
import java.util.Queue;

import javax.swing.plaf.basic.BasicInternalFrameTitlePane.MaximizeAction;

public class CreateBinaryTree {
	 
	Node root;
	
	public static void main(String[] args) {
		CreateBinaryTree tree = new CreateBinaryTree();
		tree.root = new Node(12);
		tree.root.left = new Node(3);
		tree.root.right = new Node(14);
		tree.root.left.left = new Node(2);
		tree.root.left.right = new Node(9);
		tree.root.right.left = new Node(13);
		tree.root.right.right = new Node(15);
		int maxRes = findMaxInBinaryTree(tree.root);
		System.out.println("Value of max result:"+ maxRes);
		int res = findMinInBinaryTree(tree.root);
		System.out.println("Value of min element is====:"+ res);
		levelOrderTraversal(tree.root);
		System.out.println("Size of binary tree:"+ sizeOfBinaryTree(tree.root));
		System.out.println("deepest node"+deepestNodeInBinaryTree(tree.root).data);
		//deleteNode(1, tree.root);
		System.out.println("Last data========");
		levelOrderTraversal(tree.root);
		int width = maxwidthOfTree(tree.root);
		System.out.println("Width of tree is=====:"+ width);
		boolean result = isBinarySearchTree(tree.root);
		System.out.println("Is BST:"+ result);
		
		
	}

	static int findMaxInBinaryTree(Node root) {
		if(root == null) {
			return Integer.MIN_VALUE;
		}
		int result = root.data;
		int lres =  findMaxInBinaryTree(root.left);
		int rres = findMaxInBinaryTree(root.right);
		if(lres>result) {
			result = lres;
		} 
		if(rres > result) {
			result = rres;
		}
		return result;
		
	}
	
	static int findMinInBinaryTree(Node root) {
		
		if(root == null) {
			return Integer.MAX_VALUE;
		}
		int result = root.data;
		int lres =  findMinInBinaryTree(root.left);
		int rres = findMinInBinaryTree(root.right);
		if(lres<result) {
			result = lres;
		} 
		if(rres < result) {
			result = rres;
		}
		return result;
	}
	static void levelOrderTraversal(Node root) {
		Queue<Node> queue = new ArrayDeque<>();
		queue.add(root);
		while(!queue.isEmpty()) {
			Node curr = queue.poll();
			System.out.println(curr.data);
			if(curr.left!=null) {
				queue.add(curr.left);
			}
			if(curr.right!=null) {
				queue.add(curr.right);
			}
		}
	}
	
	static int sizeOfBinaryTree(Node root) {
		int left =0;
		int right =0;
		if(root==null) {
			return 0;
		}
		if(root.left!=null) {
		 left =	sizeOfBinaryTree(root.left);
		}
		if(root.right!=null) {
		 right =	sizeOfBinaryTree(root.right);
		}
		return left+right+1;
	}
	
	static Node deepestNodeInBinaryTree(Node root) {
		Queue<Node> queue = new ArrayDeque<>();
		queue.add(root);
		Node curr = null ;
		while(!queue.isEmpty()) {
			curr = queue.poll();
			System.out.println(curr.data);
			if(curr.left!=null) {
				queue.add(curr.left);
			}
			if(curr.right!=null) {
				queue.add(curr.right);
			}
		}
		return curr;
		
	}
	
	static void deleteNode(int num , Node root) {
		
		Queue<Node> queue = new ArrayDeque<>();
		queue.add(root);
		Node last = deepestNodeInBinaryTree(root);
		while(!queue.isEmpty()) {
			Node curr = queue.poll();
			
			System.out.println("After deleting:"+curr.data);
			if(curr.left!=null) {
				if(curr.left.data == num){
				Node tmpLeft = curr.left.left;
				Node tmpRight = curr.left.right;
				curr.left = last;
				last.left = tmpLeft;
				last.right =  tmpRight;
				}
				queue.add(curr.left);
			}
			if(curr.right!=null) {
				if(curr.right.data == num){
					Node tmpLeft = curr.right.left;
					Node tmpRight = curr.right.right;
					curr.right = last;
					last.left = tmpLeft;
					last.right =  tmpRight;
					}
				queue.add(curr.right);
			}
		}
		
	}
	
	static int maxwidthOfTree(Node root) {
		
		Queue<Node> queue = new ArrayDeque<>();
		queue.add(root);
		int count = 0;
		int result = 0;
		while(!queue.isEmpty()) {
			
			count = queue.size();
			if(count>result) {
				result = count;
			}
			while(count>0) {
			Node temp = queue.poll();
			if(temp.left!=null) {
				queue.add(temp.left);
				}
			if(temp.right!=null) {
				queue.add(temp.right);
			}
			count--;
		}
		}
		return result;
	}
	
		static boolean isBinarySearchTree(Node root){
		if(root==null){
			return true;
		}
		if(root.getLeft()!=null && findMaxInBinaryTree(root.left)>root.getData()){
			return false;
		}
		if(root.getRight()!=null && findMinInBinaryTree(root.right)<root.getData()){
			return false;
		}
		if(!isBinarySearchTree(root.getLeft()) || !isBinarySearchTree(root.getRight())){
			return false;
		}
		return true;
	}
}
