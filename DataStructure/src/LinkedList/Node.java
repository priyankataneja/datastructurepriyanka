package LinkedList;

public class Node {
	
	public int data;
	Node next;
	
	public int getData() {
		return data;
	}

	public void setData(int data) {
		this.data = data;
	}

	public Node getNext() {
		return next;
	}

	public void setNext(Node next) {
		this.next = next;
	}

	Node(int data){
		this.data = data;
	}

}
