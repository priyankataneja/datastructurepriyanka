package LinkedList;

import java.util.HashSet;

public class ListOperation {

	Node head;
	Node curr;
	int counter = 0;
	int n = 1;
	int i=1;
	int k=1;
	int p =0;

	void push(int data){
		
		Node node = new Node(data);
		
		if(head==null){
			head = node;
			head.next = null;
			curr = head;
		} else {
			
			curr.next = node;
			curr = node;
			curr.next = null;
		}
	}
	
	void printList(){
		Node temp = head;
		while(temp != null){
			System.out.println("List data is:"+temp.data);
			temp = temp.next;
		}
		
	}
	
	void removeDuplicatesFromUnsorted(Node head) {
		Node ptr1;
		Node ptr2 = null;
		ptr1 = head;
		while(ptr1!=null  && ptr1.next!=null) {
			ptr2 = ptr1;
			
			while(ptr2.next!=null) {
				if(ptr1.data == ptr2.next.data){
					ptr2.next = ptr2.next.next;
					
				} else {
					ptr2 = ptr2.next;
				}
			}
			ptr1 = ptr1.next;
		}
		
	}
	
	int countListLength(){
		int length =0;
		Node temp = head;
		while(temp != null){
			length++;
			System.out.println(temp.data);
			temp = temp.next;
		}
		return length;
	}
	
	void insertAtSpecieficPosition(int data , int position){
		
		Node temp = head;
		Node node = new Node(data);
		temp = head;
		int i = 1;
		while(i!= position-1){
			i++;
			temp = temp.next;
		}
		Node nextNode = temp.next;
		temp.next  = node;
		node.next = nextNode;	
		
	}
	
	void deleteAtSpecieficPosition(int position){
		Node prev;
		Node temp = head;
		int i = 1;
		int len = countListLength();
		if(head==null){
			System.out.println("List is empty");
		} else if(len<position){
			System.out.println("Not enough element to delete");
		} else{
		while(i!=position-1){
			i++;
			temp = temp.next;
		}
		prev = temp;
		temp = temp.next.next;
		prev.next = temp;
	}
		
		
	}
	
	
	void findNthElementEndRecursion(Node head, int position) {
		
		// Second Approach using formula
		
		int length = countListLength();
		int pos = length-position+1;
		if(k==pos){
			System.out.println("data is==============:"+ head.data);
			return;
		}
		k++;
	   findNthElementEndRecursion(head.next, position);
	 
		
	}
	
	void findNthElementUsingRecursion(Node head, int position){
		
		// Third Approach using Recursion
		
		if(head==null){
			return;
		}
		findNthElementUsingRecursion(head.next, position);
		
		if(++p== position){
			System.out.println("value of item is============"+ head.data);
			return;
		}
		
	}
	
	
	int findFromEnd(int position){
		// First Approach using Two pointers and iteratively
		Node advancePtr = head;
		Node backPtr = head;
		while(i<position){
			i++;
			advancePtr = advancePtr.next;
		}
		while(advancePtr.next != null){
			
			advancePtr = advancePtr.next;
			backPtr= backPtr.next;
		}
		System.out.println("Value is==========:"+ backPtr.data);
		return backPtr.data;
		
	}
	
	
	void printListRecursively(Node head){
		if(head==null){
			return;
		}
		printListRecursively(head.next);
		System.out.println("Printing List data reverse:"+ head.data);
		}
	
	void sortListData(Node head){
			if(head==null){
				return;
			}
			
	}
	
	void findMiddle(Node head){
		Node slowptr = head;
		Node fastptr = head;
		while(fastptr != null && fastptr.next!=null){
			slowptr = slowptr.next;
			fastptr = fastptr.next.next;
		}
		System.out.println("Middle Value is========:" + slowptr.data);
	}
	
	void checkPalindrome(Node head){
		Node finalNode = null;
		Node slowptr = head;
		Node fastptr = head;
		while(fastptr != null && fastptr.next!=null){
			slowptr = slowptr.next;
			fastptr = fastptr.next.next;
		}
		
		while(slowptr.next!=null){
			Node nxt = slowptr.next;
			slowptr.next = finalNode;
			finalNode = slowptr;
			slowptr = nxt;
			
		}
		
	}

}
