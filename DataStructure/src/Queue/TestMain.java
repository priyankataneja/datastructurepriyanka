package Queue;

public class TestMain {
	
	public static void main(String[] args) {
		BasicQueueOperation bo = new BasicQueueOperation();
		bo.enqueue(10);
		bo.enqueue(20);
		bo.enqueue(30);
		bo.enqueue(40);
		bo.printQueueData();
		bo.dequeue();
		bo.printQueueData();
		
	}

}
