package Queue;

public class BasicQueueOperation {

	public int size = 5;
	int arr[] = new int[size];
	int front = 0 ;
	int rear = 0;
	int length=0;
	public void enqueue(int data){
		
		if(length == size){
			System.out.println("Queue Full");
		} else {
			arr[rear] = data;
			rear++; // rear =1;
			length++;
			
		}
		
	}
	
	public void dequeue(){
		
		if(length==0){
			System.out.println("Queue Empty");
		} else {
			int item = arr[front];
			front++;
			System.out.println("Item deleted========:"+ item);
			length = length -1;
			
		}
		
	}
	
	public void printQueueData(){
		for(int i = front; i<=length;i++){
			System.out.println(arr[i]);
		}
	}
}
